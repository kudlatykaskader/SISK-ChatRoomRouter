#include "ServerThread.h"
#include "Message.h"
ServerThread::ServerThread()
{
	this->ClientList = std::make_shared<std::vector<Client*>>();
}

ServerThread::~ServerThread()
{
	//dtor
}

void ServerThread::ServerThreadWorker()
{
	sf::TcpListener listener;
	listener.setBlocking(false);
	while(listener.listen(5000) != sf::Socket::Done)
	{
		if(ExitFlag)
			break;
		listener.close();
		sf::sleep(sf::seconds(1));
		std::cout << "Retrying binding procedure" << std::endl;
	}
	Client *cl = new Client();
	sf::SocketSelector selector;
	selector.add(listener);
	while(!ExitFlag)
	{
		if (selector.wait(sf::seconds(1)))
		{
			if(selector.isReady(listener))
			{
				if (listener.accept(cl->Socket) == sf::Socket::Done)
				{
					cl->Send(std::string("Welcome to SISK-KW64F Alarm System!\n"));
					//char buffer[10];
					//std::size_t received;
					//cl->Socket.receive(buffer, 10, received);
					cl->SetName(this->random_string(6));
					cl->room_name = this->random_string(6);
					this->ClientList->push_back(cl);
					std::cout << "Accepted connection from " << cl->Socket.getRemoteAddress() << " with initial name: " << cl->GetName() << std::endl;
					selector.add(cl->Socket);
					cl = new Client();
				}
			}
			else
			{
				for(int i = 0; i < ClientList->size(); i++)
				{
					if (selector.isReady(ClientList->at(i)->Socket))
					{
						char buffer[100];
						std::size_t received;
						for(int l = 0; l < 100; l++)
							buffer[l] = '\0';
						auto status = ClientList->at(i)->Socket.receive(buffer, 100, received);
						if(status == sf::Socket::Status::Done)
						{
							if(buffer[received-1] == '\n')
								buffer[received-1] = '\0';
							std::cout << "Message \"" << received << ":" << buffer << "\" from client: " << ClientList->at(i)->GetName() << std::endl;
							Message msg = this->GetMesssage(std::string(buffer));
							this->HandleMessage(msg, ClientList->at(i));
						}
						else if (status == sf::Socket::Status::Disconnected)
						{
							std::cout << "Removing disconnected client " << ClientList->at(i)->GetName() << std::endl;
							selector.remove(ClientList->at(i)->Socket);
							this->ClientList->erase(this->ClientList->begin() + i);

						}
					}
				}
			}
		}
		else
		{

		}
	}
	printf("Shutting down server listener\r\n");
	for(int i = 0; i < this->ClientList->size(); i++)
	{
		std::cout << "Disconnecting client: " << ClientList->at(i)->GetName() << std::endl;
		ClientList->at(i)->Socket.disconnect();
	}
	listener.close();
	selector.clear();
	std::cout << "Shutting down server thread!" << std::endl;
}

std::thread ServerThread::GetThreadHandler()
{
	return std::thread( [this] { this->ServerThreadWorker(); } );
}

std::shared_ptr<std::vector<Client*>> ServerThread::GetClientList()
{
	return std::shared_ptr<std::vector<Client*>>(ClientList);
}

std::string ServerThread::random_string( size_t length )
{
	auto randchar = []() -> char
	{
		const char charset[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";
		const size_t max_index = (sizeof(charset) - 1);
		return charset[ rand() % max_index ];
	};
	std::string str(length,0);
	std::generate_n( str.begin(), length, randchar );
	return str;
}

Message ServerThread::GetMesssage(std::string load)
{
	Message msg;
	std::size_t found = load.find(":");
	if (found == std::string::npos || found == load.size() || found == 0)
	{
		msg.valid = false;
		msg.error_string = "Error::Delimeter ':' not in middle of message";
	}
	else
	{
		msg.valid = true;
		msg.command = load.substr(0, found);
		msg.value = load.substr(found+1, load.size());
	}
	return msg;
}

void ServerThread::HandleMessage(Message msg, Client *sender)
{
	if(msg.valid == false)
	{
		sender->Send(msg.error_string);
	}
	else
	{
		if(msg.command == "room_create")
		{
			this->RoomCreateMessageHandle(msg, sender);
		}
		else if(msg.command == "room_join")
		{
			this->RoomJoinMessageHandle(msg, sender);
		}
		else if(msg.command == "name_set")
		{
			this->NameSetMessageHandle(msg, sender);
		}
		else if(msg.command == "list_users")
		{
			this->ListUsersMessageHandle(msg, sender);
		}
		else if(msg.command == "list_chats")
		{
			this->ListChatsMessageHandle(msg, sender);
		}
		else if(msg.command == "room_send")
		{
			this->RoomSendMessageHandle(msg, sender);
		}
		else
		{
			sender->Send(std::string("Error::Command Unrecognized command"));
		}
	}
}

void ServerThread::NameSetMessageHandle(Message msg, Client *sender)
{
	bool name_taken = false;
	for(int i = 0; i < ClientList->size(); i++)
	{
		if(ClientList->at(i)->GetName() == msg.value)
		{
			name_taken = true;
		}
	}
	if(name_taken)
	{
		sender->Send("Error:: This name is already taken '" + msg.value + "'");
	}
	else
	{
		sender->SetName(msg.value);
		sender->name_set = true;
		sender->Send("INFO:: Your name has been changed to: '" + msg.value + "'");
	}
}
void ServerThread::RoomCreateMessageHandle(Message msg, Client *sender)
{
	if ( std::find(this->chat_rooms.begin(), this->chat_rooms.end(), msg.value) != this->chat_rooms.end() )
	{
		sender->Send("Error:: This chat room already exist");
	}
	else
	{

		for(int i = 0; i < ClientList->size(); i++)
		{
			ClientList->at(i)->Send("INFO:: New room created '"+msg.value+"'");
			this->chat_rooms.push_back(msg.value);
		}
	}
}
void ServerThread::RoomJoinMessageHandle(Message msg, Client *sender)
{
	if(sender->name_set == true)
	{
		if ( std::find(this->chat_rooms.begin(), this->chat_rooms.end(), msg.value) != this->chat_rooms.end() )
		{
			sender->room_name = msg.value;
			sender->Send("INFO:: You have joined room '" + msg.value + "'");
			sender->chat_set = true;
		}
		else
		{
			sender->Send("Error:: This chat room does not exist");
		}

	}
	else
	{
		sender->Send("Error:: Set your name first!");
	}
}
void ServerThread::ListUsersMessageHandle(Message msg, Client *sender)
{
	if(msg.value == "all")
	{
		for(int i = 0; i < ClientList->size(); i++)
		{
			sender->Send(i + "Name: " + ClientList->at(i)->GetName() + ", Active chatroom: " + ClientList->at(i)->room_name);
		}
	}
	else if(msg.value == "room")
	{
		if(sender->name_set == false)
		{
			sender->Send("Error:: Set your name first!");
		}
		else if(sender->chat_set == false)
		{
			sender->Send("Error:: You are not joined any room! Join or create one");
		}
		else
		{
			for(int i = 0; i < ClientList->size(); i++)
			{
				if(ClientList->at(i)->room_name == sender->room_name)
					sender->Send(i + "Name: " + ClientList->at(i)->GetName() + ", Active chatroom: " + ClientList->at(i)->room_name);
			}
		}
	}
	else
	{
		sender->Send(std::string("Error::Command Unrecognized command"));
	}

}
void ServerThread::ListChatsMessageHandle(Message msg, Client *sender)
{
	for(std::string chat : this->chat_rooms)
	{
		sender->Send(chat + "\n");
	}
}

void ServerThread::RoomSendMessageHandle(Message msg, Client *sender)
{
	if(sender->chat_set == false)
	{
		sender->Send(std::string("Error:: You are not joined any room! Join or create one"));
	}
	else if(sender->name_set == false)
	{
		sender->Send(std::string("Error:: Set your name first!"));
	}
	else
	{
		std::cout << "Sending: \n" << msg.value << "\n TO: " << std::endl;
		for(int i = 0; i < ClientList->size(); i++)
		{
			if(ClientList->at(i)->room_name == sender->room_name)
			{
				std::cout << ClientList->at(i)->GetName() << ", ";
				ClientList->at(i)->Send(sender->GetName()+":"+msg.value);
			}
		}
		std::cout << std::endl;
	}

}
