#include "CliThread.h"

CliThread::CliThread()
{
}

CliThread::CliThread(std::shared_ptr<std::vector<Client*>> val)
{
    this->ClientList = val;
}

CliThread::~CliThread()
{
    //dtor
}

void CliThread::CliThreadWorker()
{
    std::vector<std::string> command_set;
    unsigned int word = 0;
    while(!ExitFlag)
    {
        char command_c[256];
        std::cin.getline(command_c, 256);
        std::string command(command_c);
        if(command.size() < 1)
            continue;
        std::cout << "Command is " << command << std::endl;
        std::stringstream ss(command);
        std::vector<std::string> command_set;
        std::string temp_command_word;
        while (ss >> temp_command_word)
            command_set.push_back(temp_command_word);
        ExecuteCommand(command_set);
        command_set.clear();
    }
}

void CliThread::ExecuteCommand(std::vector<std::string> command)
{
    if(command.at(0) == "broadcast" )
    {
        for(int i = 0; i < ClientList->size(); i++)
        {
            std::cout << "Sending message to client at index: " << i << std::endl;
            ClientList->at(i)->Send(command.at(1));
        }
    }
    if(command.at(0) == "broadcast_file" )
    {
        std::ifstream ifs(command.at(1), std::ios::binary);
        std::vector<char> data;
        if(ifs.is_open())
        {
            std::vector<char> buffer((
                std::istreambuf_iterator<char>(ifs)),
                (std::istreambuf_iterator<char>()));
            std::cout << "File readed, size: " << buffer.size() << std::endl;
            data = buffer;
        }
        unsigned int data_size = 100;
        char arr[data_size];
        unsigned int data_pointer = data_size;
        while(data_pointer < data.size()-data_size)
        {
            std::copy(data.begin()+data_pointer-data_size, data.begin()+data_pointer, arr);
            data_pointer += data_size;
            for(int i = 0; i < ClientList->size(); i++)
            {
                ClientList->at(i)->Send(arr);
            }
        }


        std::cout <<"Done!" << std::endl;
    }
}
std::thread CliThread::GetThreadHandler()
{
    return std::thread( [this] { this->CliThreadWorker(); } );
}
