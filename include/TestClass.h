#ifndef TESTCLASS_H
#define TESTCLASS_H


class TestClass
{
    public:
        TestClass();
        virtual ~TestClass();

        unsigned int Getval() { return val; }
        void Setval(unsigned int val) { val = val; }

    protected:

    private:
        unsigned int val;
};

#endif // TESTCLASS_H
