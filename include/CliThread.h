#ifndef CLITHREAD_H
#define CLITHREAD_H
#include <thread>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include "Client.h"

class CliThread
{
    public:
        CliThread();
        CliThread(std::shared_ptr<std::vector<Client*>> val);
        virtual ~CliThread();
        void CliThreadWorker();
        std::thread GetThreadHandler();
        std::shared_ptr<std::vector<Client*>> ClientList;
        bool ExitFlag = false;
        void ExecuteCommand(std::vector<std::string> command);
    protected:

    private:
};

#endif // CLITHREAD_H
